# -*- coding: utf-8 -*-
# FreeCAD tools of the LopoliExport workbench
# (c) 2001 Juergen Riegel
# License LGPL

import FreeCAD, FreeCADGui, Mesh
import plywoodexport



class CmdHelloWorld:
    def Activated(self):

        a = FreeCADGui.Selection.getSelection()[0]
        print a
        if type(a) == Mesh.Feature:
            print 'Mesh'
            ply = plywoodexport.PlywoodExport(a.Mesh, scale = 1, active_doc = False)
            print ply
        
            if ply:
                print 'reconstructing...'
                ply.proceed_geometry()
                ply.proceed_cut_and_fuse()
                ply.reconstruct_3d()
                print 'done'
    def IsActive(self):
        return True
    def GetResources(self):
        return {'Pixmap': 'freecad', 'MenuText': 'Get mesh', 'ToolTip': 'Print Hello World'}

class CmdHelloWorld2:
    def Activated(self):
        FreeCAD.Console.PrintMessage("Reconstruct\n")
        print ply
            
    def IsActive(self):
        return True
    def GetResources(self):
        return {'Pixmap': 'freecad', 'MenuText': 'Reconstruct', 'ToolTip': 'Print Hello World'}

FreeCADGui.addCommand('LopoliExport_HelloWorld', CmdHelloWorld())
FreeCADGui.addCommand('LopoliExport_HelloWorld2', CmdHelloWorld2())

