# -*- coding: utf-8 -*-
# LopoliExport gui init module
# (c) 2001 Juergen Riegel
# License LGPL

class LopoliExportWorkbench ( Workbench ):
    "LopoliExport workbench object"
    Icon = FreeCAD.getHomePath() + "Mod/LopoliExport/Resources/icons/LopoliExportWorkbench.svg"
    MenuText = "LopoliExport"
    ToolTip = "LopoliExport workbench"
    
    def Initialize(self):
        # load the module
        import LopoliExportGui
        self.appendToolbar('LopoliExport',['LopoliExport_HelloWorld', 'LopoliExport_HelloWorld2'])
        self.appendMenu('LopoliExport',['LopoliExport_HelloWorld'])

        commandsList = [
            'explode',
            'simulation',
            'collision'
            ]
        
        self.appendToolbar('LopoliExport', commandsList)
        self.appendMenu('LopoliExport', commandsList)

    def Activated(self):
        import FreeCAD
        import LopoliFeature
        doc = FreeCAD.activeDocument()
        a = FreeCAD.ActiveDocument.addObject("App::FeaturePython", "LopoliFeature")
        LopoliFeature.LopoliObject(a)
        LopoliFeature.ViewProviderBox(a.ViewObject)
        print doc
        
    def GetClassName(self):
        return "Gui::PythonWorkbench"

Gui.addWorkbench(LopoliExportWorkbench())
