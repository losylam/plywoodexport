#!/usr/bin/env/python
# coding:utf-8

"""Usage:
  plywoodexport.py [--thickness <mm> --nslots <nslots> --no-hershey --clear-radius <mm> --drill-radius <mm>] INPUT OUTPUT 
  plywoodexport.py [--thickness <mm> --nslots <nslots> --no-hershey --clear-radius <mm> --drill-radius <mm> --debug <out.fcstd>] INPUT OUTPUT 
  plywoodexport.py [--thickness <mm> --nslots <nslots> --no-hershey --clear-radius <mm> --drill-radius <mm> --reconstruct <out.fcstd>] INPUT OUTPUT 
  plywoodexport.py [--thickness <mm> --nslots <nslots> --no-hershey --clear-radius <mm> --drill-radius <mm> --stat] INPUT 
  plywoodexport.py [-t <mm> -n <nslots> -r <mm> -d <mm>] INPUT OUTPUT
  plywoodexport.py --test
  plywoodexport.py -h

A FreeCAD script generating wood enveloppe with slot assembly 
from STL Files

Arguments: 
  INPUT                    input file (.stl)
  OUTPUT                   output file (.svg)

Options:
  -h --help                Display help
  -t, --thickness <mm>     Material thickness in mm [default: 5]
  -n, --nslots <nslots>    Number of slots by edge [default: 4]
  -r, --clear-radius <mm>  Radius of the clear radius (none if -1) [default: -1]
  -d, --drill-radius <mm>  Radius if the drill tool (none if -1) [default: -1]
  --reconstruct <out>      Reconstruction export
  --debug <out>            Debug export
  --test                   Run test programm
"""

import sys
import math
import logging
import xml.etree.ElementTree as ET

from docopt import docopt
from tqdm import tqdm
import coloredlogs

FREECADPATH = '/usr/lib/freecad/lib'
FONTPATH = '/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf'

sys.path.append(FREECADPATH)
import FreeCAD
import Mesh
import MeshPart
import Part
import Draft

logging.basicConfig(format='%(asctime)-15s %(levelname)s: %(message)s',
                    level=logging.DEBUG)
coloredlogs.install(level='DEBUG', fmt= '%(asctime)-15s %(levelname)s: %(message)s')

PI = math.pi
V_O = FreeCAD.Vector(0,0,0)
V_X = FreeCAD.Vector(1,0,0)
V_Y = FreeCAD.Vector(0,1,0)
V_Z = FreeCAD.Vector(0,0,1)

TEST = False

EMPTY_SVG="""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!-- Cree avec PlywoodExport -->
<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   width="210mm"
   height="297mm"
   id="svg2"
   version="1.1">
  <defs
     id="defs4" />
  <g
     inkscape:label="PlywoodExport"
     inkscape:groupmode="layer"
     id="layer1">
"""

futural = ["-8 8","-5 5 M 0 -12 L 0 2 M 0 7 L -1 8 L 0 9 L 1 8 L 0 7","-8 8 M -4 -12 L -4 -5 M 4 -12 L 4 -5","-10 11 M 1 -16 L -6 16 M 7 -16 L 0 16 M -6 -3 L 8 -3 M -7 3 L 7 3","-10 10 M -2 -16 L -2 13 M 2 -16 L 2 13 M 7 -9 L 5 -11 L 2 -12 L -2 -12 L -5 -11 L -7 -9 L -7 -7 L -6 -5 L -5 -4 L -3 -3 L 3 -1 L 5 0 L 6 1 L 7 3 L 7 6 L 5 8 L 2 9 L -2 9 L -5 8 L -7 6","-12 12 M 9 -12 L -9 9 M -4 -12 L -2 -10 L -2 -8 L -3 -6 L -5 -5 L -7 -5 L -9 -7 L -9 -9 L -8 -11 L -6 -12 L -4 -12 L -2 -11 L 1 -10 L 4 -10 L 7 -11 L 9 -12 M 5 2 L 3 3 L 2 5 L 2 7 L 4 9 L 6 9 L 8 8 L 9 6 L 9 4 L 7 2 L 5 2","-13 13 M 10 -3 L 10 -4 L 9 -5 L 8 -5 L 7 -4 L 6 -2 L 4 3 L 2 6 L 0 8 L -2 9 L -6 9 L -8 8 L -9 7 L -10 5 L -10 3 L -9 1 L -8 0 L -1 -4 L 0 -5 L 1 -7 L 1 -9 L 0 -11 L -2 -12 L -4 -11 L -5 -9 L -5 -7 L -4 -4 L -2 -1 L 3 6 L 5 8 L 7 9 L 9 9 L 10 8 L 10 7","-5 5 M 0 -10 L -1 -11 L 0 -12 L 1 -11 L 1 -9 L 0 -7 L -1 -6","-7 7 M 4 -16 L 2 -14 L 0 -11 L -2 -7 L -3 -2 L -3 2 L -2 7 L 0 11 L 2 14 L 4 16","-7 7 M -4 -16 L -2 -14 L 0 -11 L 2 -7 L 3 -2 L 3 2 L 2 7 L 0 11 L -2 14 L -4 16","-8 8 M 0 -6 L 0 6 M -5 -3 L 5 3 M 5 -3 L -5 3","-13 13 M 0 -9 L 0 9 M -9 0 L 9 0","-4 4 M 1 5 L 0 6 L -1 5 L 0 4 L 1 5 L 1 7 L -1 9","-13 13 M -9 0 L 9 0","-4 4 M 0 4 L -1 5 L 0 6 L 1 5 L 0 4","-11 11 M 9 -16 L -9 16","-10 10 M -1 -12 L -4 -11 L -6 -8 L -7 -3 L -7 0 L -6 5 L -4 8 L -1 9 L 1 9 L 4 8 L 6 5 L 7 0 L 7 -3 L 6 -8 L 4 -11 L 1 -12 L -1 -12","-10 10 M -4 -8 L -2 -9 L 1 -12 L 1 9","-10 10 M -6 -7 L -6 -8 L -5 -10 L -4 -11 L -2 -12 L 2 -12 L 4 -11 L 5 -10 L 6 -8 L 6 -6 L 5 -4 L 3 -1 L -7 9 L 7 9","-10 10 M -5 -12 L 6 -12 L 0 -4 L 3 -4 L 5 -3 L 6 -2 L 7 1 L 7 3 L 6 6 L 4 8 L 1 9 L -2 9 L -5 8 L -6 7 L -7 5","-10 10 M 3 -12 L -7 2 L 8 2 M 3 -12 L 3 9","-10 10 M 5 -12 L -5 -12 L -6 -3 L -5 -4 L -2 -5 L 1 -5 L 4 -4 L 6 -2 L 7 1 L 7 3 L 6 6 L 4 8 L 1 9 L -2 9 L -5 8 L -6 7 L -7 5","-10 10 M 6 -9 L 5 -11 L 2 -12 L 0 -12 L -3 -11 L -5 -8 L -6 -3 L -6 2 L -5 6 L -3 8 L 0 9 L 1 9 L 4 8 L 6 6 L 7 3 L 7 2 L 6 -1 L 4 -3 L 1 -4 L 0 -4 L -3 -3 L -5 -1 L -6 2","-10 10 M 7 -12 L -3 9 M -7 -12 L 7 -12","-10 10 M -2 -12 L -5 -11 L -6 -9 L -6 -7 L -5 -5 L -3 -4 L 1 -3 L 4 -2 L 6 0 L 7 2 L 7 5 L 6 7 L 5 8 L 2 9 L -2 9 L -5 8 L -6 7 L -7 5 L -7 2 L -6 0 L -4 -2 L -1 -3 L 3 -4 L 5 -5 L 6 -7 L 6 -9 L 5 -11 L 2 -12 L -2 -12","-10 10 M 6 -5 L 5 -2 L 3 0 L 0 1 L -1 1 L -4 0 L -6 -2 L -7 -5 L -7 -6 L -6 -9 L -4 -11 L -1 -12 L 0 -12 L 3 -11 L 5 -9 L 6 -5 L 6 0 L 5 5 L 3 8 L 0 9 L -2 9 L -5 8 L -6 6","-4 4 M 0 -3 L -1 -2 L 0 -1 L 1 -2 L 0 -3 M 0 4 L -1 5 L 0 6 L 1 5 L 0 4","-4 4 M 0 -3 L -1 -2 L 0 -1 L 1 -2 L 0 -3 M 1 5 L 0 6 L -1 5 L 0 4 L 1 5 L 1 7 L -1 9","-12 12 M 8 -9 L -8 0 L 8 9","-13 13 M -9 -3 L 9 -3 M -9 3 L 9 3","-12 12 M -8 -9 L 8 0 L -8 9","-9 9 M -6 -7 L -6 -8 L -5 -10 L -4 -11 L -2 -12 L 2 -12 L 4 -11 L 5 -10 L 6 -8 L 6 -6 L 5 -4 L 4 -3 L 0 -1 L 0 2 M 0 7 L -1 8 L 0 9 L 1 8 L 0 7","-13 14 M 5 -4 L 4 -6 L 2 -7 L -1 -7 L -3 -6 L -4 -5 L -5 -2 L -5 1 L -4 3 L -2 4 L 1 4 L 3 3 L 4 1 M -1 -7 L -3 -5 L -4 -2 L -4 1 L -3 3 L -2 4 M 5 -7 L 4 1 L 4 3 L 6 4 L 8 4 L 10 2 L 11 -1 L 11 -3 L 10 -6 L 9 -8 L 7 -10 L 5 -11 L 2 -12 L -1 -12 L -4 -11 L -6 -10 L -8 -8 L -9 -6 L -10 -3 L -10 0 L -9 3 L -8 5 L -6 7 L -4 8 L -1 9 L 2 9 L 5 8 L 7 7 L 8 6 M 6 -7 L 5 1 L 5 3 L 6 4","-9 9 M 0 -12 L -8 9 M 0 -12 L 8 9 M -5 2 L 5 2","-11 10 M -7 -12 L -7 9 M -7 -12 L 2 -12 L 5 -11 L 6 -10 L 7 -8 L 7 -6 L 6 -4 L 5 -3 L 2 -2 M -7 -2 L 2 -2 L 5 -1 L 6 0 L 7 2 L 7 5 L 6 7 L 5 8 L 2 9 L -7 9","-10 11 M 8 -7 L 7 -9 L 5 -11 L 3 -12 L -1 -12 L -3 -11 L -5 -9 L -6 -7 L -7 -4 L -7 1 L -6 4 L -5 6 L -3 8 L -1 9 L 3 9 L 5 8 L 7 6 L 8 4","-11 10 M -7 -12 L -7 9 M -7 -12 L 0 -12 L 3 -11 L 5 -9 L 6 -7 L 7 -4 L 7 1 L 6 4 L 5 6 L 3 8 L 0 9 L -7 9","-10 9 M -6 -12 L -6 9 M -6 -12 L 7 -12 M -6 -2 L 2 -2 M -6 9 L 7 9","-10 8 M -6 -12 L -6 9 M -6 -12 L 7 -12 M -6 -2 L 2 -2","-10 11 M 8 -7 L 7 -9 L 5 -11 L 3 -12 L -1 -12 L -3 -11 L -5 -9 L -6 -7 L -7 -4 L -7 1 L -6 4 L -5 6 L -3 8 L -1 9 L 3 9 L 5 8 L 7 6 L 8 4 L 8 1 M 3 1 L 8 1","-11 11 M -7 -12 L -7 9 M 7 -12 L 7 9 M -7 -2 L 7 -2","-4 4 M 0 -12 L 0 9","-8 8 M 4 -12 L 4 4 L 3 7 L 2 8 L 0 9 L -2 9 L -4 8 L -5 7 L -6 4 L -6 2","-11 10 M -7 -12 L -7 9 M 7 -12 L -7 2 M -2 -3 L 7 9","-10 7 M -6 -12 L -6 9 M -6 9 L 6 9","-12 12 M -8 -12 L -8 9 M -8 -12 L 0 9 M 8 -12 L 0 9 M 8 -12 L 8 9","-11 11 M -7 -12 L -7 9 M -7 -12 L 7 9 M 7 -12 L 7 9","-11 11 M -2 -12 L -4 -11 L -6 -9 L -7 -7 L -8 -4 L -8 1 L -7 4 L -6 6 L -4 8 L -2 9 L 2 9 L 4 8 L 6 6 L 7 4 L 8 1 L 8 -4 L 7 -7 L 6 -9 L 4 -11 L 2 -12 L -2 -12","-11 10 M -7 -12 L -7 9 M -7 -12 L 2 -12 L 5 -11 L 6 -10 L 7 -8 L 7 -5 L 6 -3 L 5 -2 L 2 -1 L -7 -1","-11 11 M -2 -12 L -4 -11 L -6 -9 L -7 -7 L -8 -4 L -8 1 L -7 4 L -6 6 L -4 8 L -2 9 L 2 9 L 4 8 L 6 6 L 7 4 L 8 1 L 8 -4 L 7 -7 L 6 -9 L 4 -11 L 2 -12 L -2 -12 M 1 5 L 7 11","-11 10 M -7 -12 L -7 9 M -7 -12 L 2 -12 L 5 -11 L 6 -10 L 7 -8 L 7 -6 L 6 -4 L 5 -3 L 2 -2 L -7 -2 M 0 -2 L 7 9","-10 10 M 7 -9 L 5 -11 L 2 -12 L -2 -12 L -5 -11 L -7 -9 L -7 -7 L -6 -5 L -5 -4 L -3 -3 L 3 -1 L 5 0 L 6 1 L 7 3 L 7 6 L 5 8 L 2 9 L -2 9 L -5 8 L -7 6","-8 8 M 0 -12 L 0 9 M -7 -12 L 7 -12","-11 11 M -7 -12 L -7 3 L -6 6 L -4 8 L -1 9 L 1 9 L 4 8 L 6 6 L 7 3 L 7 -12","-9 9 M -8 -12 L 0 9 M 8 -12 L 0 9","-12 12 M -10 -12 L -5 9 M 0 -12 L -5 9 M 0 -12 L 5 9 M 10 -12 L 5 9","-10 10 M -7 -12 L 7 9 M 7 -12 L -7 9","-9 9 M -8 -12 L 0 -2 L 0 9 M 8 -12 L 0 -2","-10 10 M 7 -12 L -7 9 M -7 -12 L 7 -12 M -7 9 L 7 9","-7 7 M -3 -16 L -3 16 M -2 -16 L -2 16 M -3 -16 L 4 -16 M -3 16 L 4 16","-7 7 M -7 -12 L 7 12","-7 7 M 2 -16 L 2 16 M 3 -16 L 3 16 M -4 -16 L 3 -16 M -4 16 L 3 16","-8 8 M 0 -14 L -8 0 M 0 -14 L 8 0","-9 9 M -9 16 L 9 16","-4 4 M 1 -7 L -1 -5 L -1 -3 L 0 -2 L 1 -3 L 0 -4 L -1 -3","-9 10 M 6 -5 L 6 9 M 6 -2 L 4 -4 L 2 -5 L -1 -5 L -3 -4 L -5 -2 L -6 1 L -6 3 L -5 6 L -3 8 L -1 9 L 2 9 L 4 8 L 6 6","-10 9 M -6 -12 L -6 9 M -6 -2 L -4 -4 L -2 -5 L 1 -5 L 3 -4 L 5 -2 L 6 1 L 6 3 L 5 6 L 3 8 L 1 9 L -2 9 L -4 8 L -6 6","-9 9 M 6 -2 L 4 -4 L 2 -5 L -1 -5 L -3 -4 L -5 -2 L -6 1 L -6 3 L -5 6 L -3 8 L -1 9 L 2 9 L 4 8 L 6 6","-9 10 M 6 -12 L 6 9 M 6 -2 L 4 -4 L 2 -5 L -1 -5 L -3 -4 L -5 -2 L -6 1 L -6 3 L -5 6 L -3 8 L -1 9 L 2 9 L 4 8 L 6 6","-9 9 M -6 1 L 6 1 L 6 -1 L 5 -3 L 4 -4 L 2 -5 L -1 -5 L -3 -4 L -5 -2 L -6 1 L -6 3 L -5 6 L -3 8 L -1 9 L 2 9 L 4 8 L 6 6","-5 7 M 5 -12 L 3 -12 L 1 -11 L 0 -8 L 0 9 M -3 -5 L 4 -5","-9 10 M 6 -5 L 6 11 L 5 14 L 4 15 L 2 16 L -1 16 L -3 15 M 6 -2 L 4 -4 L 2 -5 L -1 -5 L -3 -4 L -5 -2 L -6 1 L -6 3 L -5 6 L -3 8 L -1 9 L 2 9 L 4 8 L 6 6","-9 10 M -5 -12 L -5 9 M -5 -1 L -2 -4 L 0 -5 L 3 -5 L 5 -4 L 6 -1 L 6 9","-4 4 M -1 -12 L 0 -11 L 1 -12 L 0 -13 L -1 -12 M 0 -5 L 0 9","-5 5 M 0 -12 L 1 -11 L 2 -12 L 1 -13 L 0 -12 M 1 -5 L 1 12 L 0 15 L -2 16 L -4 16","-9 8 M -5 -12 L -5 9 M 5 -5 L -5 5 M -1 1 L 6 9","-4 4 M 0 -12 L 0 9","-15 15 M -11 -5 L -11 9 M -11 -1 L -8 -4 L -6 -5 L -3 -5 L -1 -4 L 0 -1 L 0 9 M 0 -1 L 3 -4 L 5 -5 L 8 -5 L 10 -4 L 11 -1 L 11 9","-9 10 M -5 -5 L -5 9 M -5 -1 L -2 -4 L 0 -5 L 3 -5 L 5 -4 L 6 -1 L 6 9","-9 10 M -1 -5 L -3 -4 L -5 -2 L -6 1 L -6 3 L -5 6 L -3 8 L -1 9 L 2 9 L 4 8 L 6 6 L 7 3 L 7 1 L 6 -2 L 4 -4 L 2 -5 L -1 -5","-10 9 M -6 -5 L -6 16 M -6 -2 L -4 -4 L -2 -5 L 1 -5 L 3 -4 L 5 -2 L 6 1 L 6 3 L 5 6 L 3 8 L 1 9 L -2 9 L -4 8 L -6 6","-9 10 M 6 -5 L 6 16 M 6 -2 L 4 -4 L 2 -5 L -1 -5 L -3 -4 L -5 -2 L -6 1 L -6 3 L -5 6 L -3 8 L -1 9 L 2 9 L 4 8 L 6 6","-7 6 M -3 -5 L -3 9 M -3 1 L -2 -2 L 0 -4 L 2 -5 L 5 -5","-8 9 M 6 -2 L 5 -4 L 2 -5 L -1 -5 L -4 -4 L -5 -2 L -4 0 L -2 1 L 3 2 L 5 3 L 6 5 L 6 6 L 5 8 L 2 9 L -1 9 L -4 8 L -5 6","-5 7 M 0 -12 L 0 5 L 1 8 L 3 9 L 5 9 M -3 -5 L 4 -5","-9 10 M -5 -5 L -5 5 L -4 8 L -2 9 L 1 9 L 3 8 L 6 5 M 6 -5 L 6 9","-8 8 M -6 -5 L 0 9 M 6 -5 L 0 9","-11 11 M -8 -5 L -4 9 M 0 -5 L -4 9 M 0 -5 L 4 9 M 8 -5 L 4 9","-8 9 M -5 -5 L 6 9 M 6 -5 L -5 9","-8 8 M -6 -5 L 0 9 M 6 -5 L 0 9 L -2 13 L -4 15 L -6 16 L -7 16","-8 9 M 6 -5 L -5 9 M -5 -5 L 6 -5 M -5 9 L 6 9","-7 7 M 2 -16 L 0 -15 L -1 -14 L -2 -12 L -2 -10 L -1 -8 L 0 -7 L 1 -5 L 1 -3 L -1 -1 M 0 -15 L -1 -13 L -1 -11 L 0 -9 L 1 -8 L 2 -6 L 2 -4 L 1 -2 L -3 0 L 1 2 L 2 4 L 2 6 L 1 8 L 0 9 L -1 11 L -1 13 L 0 15 M -1 1 L 1 3 L 1 5 L 0 7 L -1 8 L -2 10 L -2 12 L -1 14 L 0 15 L 2 16","-4 4 M 0 -16 L 0 16","-7 7 M -2 -16 L 0 -15 L 1 -14 L 2 -12 L 2 -10 L 1 -8 L 0 -7 L -1 -5 L -1 -3 L 1 -1 M 0 -15 L 1 -13 L 1 -11 L 0 -9 L -1 -8 L -2 -6 L -2 -4 L -1 -2 L 3 0 L -1 2 L -2 4 L -2 6 L -1 8 L 0 9 L 1 11 L 1 13 L 0 15 M 1 1 L -1 3 L -1 5 L 0 7 L 1 8 L 2 10 L 2 12 L 1 14 L 0 15 L -2 16","-12 12 M -9 3 L -9 1 L -8 -2 L -6 -3 L -4 -3 L -2 -2 L 2 1 L 4 2 L 6 2 L 8 1 L 9 -1 M -9 1 L -8 -1 L -6 -2 L -4 -2 L -2 -1 L 2 2 L 4 3 L 6 3 L 8 2 L 9 -1 L 9 -3","-8 8 M -8 -12 L -8 9 L -7 9 L -7 -12 L -6 -12 L -6 9 L -5 9 L -5 -12 L -4 -12 L -4 9 L -3 9 L -3 -12 L -2 -12 L -2 9 L -1 9 L -1 -12 L 0 -12 L 0 9 L 1 9 L 1 -12 L 2 -12 L 2 9 L 3 9 L 3 -12 L 4 -12 L 4 9 L 5 9 L 5 -12 L 6 -12 L 6 9 L 7 9 L 7 -12 L 8 -12 L 8 9"]


def draw_svg_char(char, font, offset, vertoffset):
    style = 'stroke: #000001; fill: none'
    pathString = font[char]
    splitString = pathString.split()  
    midpoint = offset - int(splitString[0]) 
    pathString = pathString[pathString.find("M"):] #portion after first move
    trans = 'translate(' + str(midpoint) + ',' + str(vertoffset) + ')'
    text_attribs = {'style':style, 'd':pathString, 'transform':trans}
    xml_path = ET.Element('path')
    xml_path.attrib = text_attribs
    xml_str = '\n'.join(ET.tostringlist(xml_path)) + '\n'
    return midpoint + int(splitString[1]), xml_str   #new offset value

def draw_svg_text(text, font, spacing = 3):
    w = 0 # initial offset
    txt = ''
    letter_vals = [ord(q) - 32 for q in text]
    for q in letter_vals:
        if (q < 0) or (q > 95):
            w += 2*spacing
        else:
            w, txt_char = draw_svg_char(q, font, w, 0)
            txt += txt_char
    return w, txt
    
# def add_text(posx, posy, text, note_type = 'text'):
#     if note_type == 'hersey':
#         return add_text_hershey(posx, posy, text)
#     else:
#         return add_text_text(posx, posy, text)

def add_text_text(posx, posy, text, font_size=12):
    txt = ''
    txt += '<text  x="%s" y="%s" style="font-size:%spx" >\n'%(posx, 
                                                             posy, 
                                                             font_size)
    txt += text
    txt += '</text>\n'

    return txt

def add_text_hershey(posx, posy, text, font,  size):
    w, path_txt = draw_svg_text(text, font) 
    txt = ''
    txt += '<g  transform="translate(%s, %s)scale(%s)">\n'%(posx-(w/2), posy, size)
    txt += path_txt
    txt += '</g>\n'
    return txt

def print_points(part):
    for i in part.Vertexes:
        print i.Point

def print_cross(f):
    """
    for testing
    """
    for i in range(len(f.Vertexes)):
        v1 = f.Vertexes[i-1].Point - f.Vertexes[i-2].Point
        v2 = f.Vertexes[i].Point - f.Vertexes[i-1].Point
        print v2.cross(v1)
    print '///', f.normalAt(0,0)
    print

def facets_to_face(facets):
    """
    DEPRECATER
    """
    lines = []
    for i in range(len(facets.Points)):
        lines.append(Part.makeLine(facets.Points[i-1], facets.Points[i]))
    wire = Part.Wire(lines)
    return Part.Face(wire)

def get_indice(lst, point):
    for i, it in enumerate(lst):
        if point_egal(it, point):
            return i
    else:
        return -1

def point_egal(pt1, pt2, precision = 5):
    """
    
    """
    for i in range(3):
        if abs(pt1[i] - pt2[i]) > precision:
            return False
    return True
    
def arete_egale(arete_mesh, arete_part, precision = 5):
    if point_egal(arete_mesh[0], arete_part.Vertexes[0].Point, precision):
        if point_egal(arete_mesh[1], arete_part.Vertexes[1].Point, precision):
            return True
    if point_egal(arete_mesh[0], arete_part.Vertexes[1].Point, precision):
        if point_egal(arete_mesh[1], arete_part.Vertexes[0].Point, precision):
            return True
    return False

def get_biggest(lst_shapes):
    biggest = None
    max_length = 0
    for i in lst_shapes:
        if i.BoundBox.DiagonalLength > max_length:
            max_length = i.BoundBox.DiagonalLength
            biggest = i
    return biggest

def slot(thickness, angle):
    """
    get the slot thickness
    - angle in radians
    - l1 is the thickness of the cut
    - l2 the thickness of the fuse 
    """
    if (angle <= PI/2):
        l = thickness * math.sin(angle)
        l2 = (thickness/2.) * math.tan(angle/2)
        l1 = l - l2
    else:
        alpha = (PI-angle)/2
        l1 = l2 = thickness/(2*math.tan(alpha))
        #logging.debug('angle < 90 / angle: %s, l: %s'%(angle*180/PI, l1*2))
    return (l1, l2)

def slot_2(thickness, angle, valley):
    if (angle < PI/2):
        l = thickness * math.sin(angle)
        if valley:
            l1 = 0
            l2 = l
        else:
            l1 = l
            l2 = 0
    else:
        alpha = (PI-angle)/2
        l = thickness/(math.tan(alpha))
        if valley:
            l1 = 0
            l2 = l
        else:
            l1 = l
            l2 = O        
    return (l1, l2)

class PlywoodExport:
    def __init__(self, 
                 mesh, 
                 thickness = 5, 
                 nslots = 4, 
                 scale = 3.543307,
                 active_doc = True,         # create a new FreeCAD document
                 output = None,
                 clear_angle_radius = -1,   # radius of the circle cut at angle
                 type_text = 'hershey',     # use hershey text (=single line font)
                 sort_direction = "z",      # direction of face numbering
                 gap = 1,
                 drill_radius = -1):                  # space between pieces

        self.edges = {}
        self.mesh = mesh
        self.n = nslots
        self.material_thickness = thickness
        self.export_scale = scale
        self.matrix_scale = FreeCAD.Base.Matrix()
        self.matrix_scale.scale(self.export_scale, self.export_scale, self.export_scale)

        self.type_text = type_text
        self.font_size_face = 1
        self.font_size_edge = 0.5

        self.clear_angle_radius = clear_angle_radius

        self.drill_radius = drill_radius

        if sort_direction in 'xyz':
            self.sort_direction = 'xyz'.find(sort_direction)

        self.gap = gap
        if self.drill_radius > 0:
            self.gap += self.drill_radius

        self.method = 0
        if active_doc:
            self.active_document = FreeCAD.newDocument()
        if output:
            self.proceed_geometry()
            self.get_connected_faces()
            self.align()
            self.proceed_cut_and_fuse()
            self.export(output)
            self.print_summary()

    def add_text(self, posx, posy, text, size = 1):
        if self.type_text == 'hershey':
            return add_text_hershey(posx, posy, text, futural, size)
        else:
            return add_text_text(posx, posy, text, font_size = 25*size)

    def get_edges_from_facets(self):
        """
        get edges
        """
        logging.info('get edges from facets')

        lst = {}

        self.facets = []
        for f in self.mesh.Facets:
            dic_facet = {'id':f.Index, 'edges':[]}
            for i in range(len(f.PointIndices)):
                l = [f.PointIndices[i-1], f.PointIndices[i]]
                l.sort()
                s = str(l)
                if s not in lst.keys():
                    lst[s] = {'facets':[f.Index], 
                              'id':s, 
                              'id_points':l, 
                              'points':[self.mesh.Points[l[0]].Vector, self.mesh.Points[l[1]].Vector],
                              'id_faces':[],
                              'id_edge':len(lst.keys())}
                else:
                    lst[s]['facets'].append(f.Index)
                dic_facet['edges'].append(s)
            self.facets.append(dic_facet)

        self.edges = lst

    def get_edges_angle(self):
        """
        get angles from two facets for each edge
        """
        
        logging.info('get edges angles')

        for i in self.edges.values():
            if len(i['facets']) == 2:
                pt_a = i['points'][0]
                pt_b = i['points'][1]
                f1 = self.mesh.Facets[i['facets'][0]]
                f2 = self.mesh.Facets[i['facets'][1]]
                a_is_clockwise = ((get_indice(f1.Points, pt_a) - get_indice(f1.Points, pt_b)) % len(f1.Points) == 1)
                v1 = f1.Normal
                v2 = f2.Normal
                i['angle'] = round(v1.getAngle(v2), 5)
                if a_is_clockwise == (v2.cross(v1).dot(pt_b - pt_a) > 0):
                    i['valley'] = False
                else:
                    i['valley'] = True
                i['bord'] = False
            else:
                i['angle'] = 0
                i['bord'] = True

    def mesh_to_part(self, precision = 0.1):
        """
        create a face Part object for each faces
        """
        
        logging.info('create part from mesh')

        segments = self.mesh.getPlanarSegments(precision)
        self.faces = []
        f_to_fuse = []
        f_not_to_fuse = []

        # mark the facets that are fused
        for i in segments:
            for j in i:
                f_to_fuse.append(j)
                self.facets[j]['fuse'] = True
                
        for i in range(len(self.facets)):
            if i not in f_to_fuse:
                self.facets[i]['fuse'] = False
                segments.append([i])

        for i in segments:
            dic_face = {'facets':i} 
            if len(i) > 0:
                # a segment can have inner holes
                wires = MeshPart.wireFromSegment(self.mesh, i)
                if len(wires) > 0:
                    ext = get_biggest(wires)
                    wires.remove(ext)
                    for i in wires:
                        i.reverse()

                    wires.insert(0, ext)
                    dic_face['part'] = wires[0]
 #                   dic_face['part'] = Part.Face(wires[0])
                    dic_face['id'] = len(self.faces)
                    self.faces.append(dic_face)

        self.sort_faces_z()

    def sort_faces_z(self):
        lst_f = []
        for f in self.faces:
            lst_f.append((f['part'].CenterOfMass[self.sort_direction], f['id']))
        lst_f.sort()
        for i, f in enumerate(lst_f):
            self.faces[f[1]]['numero'] = i
            

    def link_edges_and_faces(self):
        """
        for add faces
        map the edges from the part object to the edges of the mesh
        
        dic_face['edges'][i] == dic_face['part'].Edges[dic_face['edges_part'][i]]
        """
        
        logging.info('link edges and faces')

        # add the list of edges to faces
        for i in self.faces:
            i['edges'] = []
            for j in i['facets']:
                for k in self.facets[j]['edges']:
                    e = self.edges[k]
                    if e['angle'] !=0 or e['bord']:
                        i['edges'].append(k)
                        e['id_faces'].append(i['id'])

        # map the edges
        for i in self.faces:
            i['edges_part'] = self.map_edges(i)

    def get_edge_part2(self, dic_face, i):
        """
        return edge object of part2
        for edge number -i- or edge named -i-
        """
        if type(i) != int:
            i = dic_face['edges'].index(i)
        return dic_face['part2'].Edges[dic_face['edges_part'][i]]

    def map_edges(self, dic_face):
        """
        for the face described by -dic_face-
        map the edges from the part object to the edges of the mesh
        """
        edges_part = []
        for i in dic_face['edges']:
            ok = True
            for j, e in enumerate(dic_face['part'].Edges):
                
                if arete_egale(self.edges[i]['points'], e) and ok:
                    if dic_face['id'] not in self.edges[i]['id_faces']:
                        self.edges[i]['id_faces'].append(dic_face['id'])
                    edges_part.append(j)
                    ok = False
            if ok:
                self.edges[i]['flat'] = True
                edges_part.append(None)
            

        if len(edges_part) != len(dic_face['edges']):
            print 'toutes les aretes n\'ont pas été trouvées pour la face %s'%dic_face['id']

        return edges_part

    def apply_transforms(self):
        """
        for each faces
        get the rotation data to 
        - point of rotation (Vector)
        - axe of rotation (Vector)
        - angle (degrees)
        and apply 
        """
        
        logging.info('apply projection xy : rotate')

        for f in self.faces:
            f['part0'] = f['part'].copy()
            t = f['transform'] = self.get_transform(f)
            if t['angle_rot'] != 0 and t['vec_rot'].Length !=0 :
                f['part'].rotate(t['point_rot'],
                                 t['vec_rot'],
                                 t['angle_rot'])

            #f['part'].translate(t['translate'])

    def get_transform(self, dic_face):
        """
        for the face described by -dic_face-
        return the rotation data 
        - point of rotation (Vector)
        - axe of rotation (Vector)
        - angle (degrees)
        """
        z = FreeCAD.Vector(0,0,1)
        n = self.mesh.Facets[dic_face['facets'][0]].Normal
        trans = {'vec_rot' : n.cross(z),
                 'angle_rot' : n.getAngle(z) * 180 / math.pi,
                 'point_rot' : dic_face['part'].CenterOfMass}
        if n[2] < 0:
            trans['translate'] = FreeCAD.Vector(500, 0, 0)
        else:
            trans['translate'] = FreeCAD.Vector(0,0,0)
        return trans
        
    def get_draft(self):
        for i in self.faces:
            p = i['part']
            lst_points = [v.Point for v in p.Vertexes]
            for j in lst_points:
                j[2] = 0.0
            i['draft'] = Draft.makeWire(lst_points, closed = True, face = True)

    def project_on_xy(self):

        logging.info('apply projection xy: translate')

        for i in self.faces:
            p = i['part']
            lst_points = [v.Point for v in p.Vertexes]
            i['z0'] = lst_points[0][2]
            for j in lst_points:
                j[2] = 0.0
            lst_points.append(lst_points[0])
            poly = Part.makePolygon(lst_points)
            w = Part.Wire(poly)
            i['part2'] = Part.Face(w)

    def get_biggest_face(self):
        max_area = 0
        i_face = -1
        for i , it in enumerate(self.faces):
            area = it['part2'].Area
            if area > max_area:
                i_face = i
                max_area = area
        return i_face

    def turn_face(self, fix_face, turn_face):
        i_edge = set(fix_face['edges']).intersection(set(turn_face['edges'])).pop()
        edge = self.edges[i_edge]

        i_edge_f = fix_face['edges'].index(i_edge)
        edge_f = fix_face['part2'].Edges[fix_face['edges_part'][i_edge_f]]
        points_f = [i.Point for i in edge_f.Vertexes]
        vec_f = points_f[1] - points_f[0]

        i_edge_t = turn_face['edges'].index(i_edge)
        edge_t = turn_face['part2'].Edges[turn_face['edges_part'][i_edge_t]]
        points_t = [i.Point for i in edge_t.Vertexes]
        vec_t = points_t[1] - points_t[0]
        gap = self.get_ep(edge)[1]*2 + self.gap
        if vec_f.cross(vec_t)[2] >0:
            fac = 1
        else:
            fac = -1
        angle = vec_t.getAngle(vec_f)
        turn_face['part2'].rotate(points_t[0], V_Z, fac * (180 - angle*180/PI))
        v_tr = points_f[1] - points_t[0]

        if vec_f.cross(V_Z).Length !=0:
            v_tr = v_tr - vec_f.cross(V_Z).normalize()*gap
        turn_face['part2'].translate(v_tr)
        turn_face['turned'] = True
        
        
    def turn_faces(self, dic_face):
        lst_to_turn = [self.faces[i] for i in dic_face['id_faces'] if not self.faces[i].has_key('turned')]
        lst_to_turn_sort = []

        for f in lst_to_turn:
            common_edge = self.get_common_edge(dic_face, f)
            lst_to_turn_sort.append((self.edges[common_edge]['angle'], f))
            
        lst_to_turn_sort.sort()
        lst_to_turn_sort.reverse()

        for f in lst_to_turn:
            self.turn_face(dic_face, f)
            self.cpt += 1

        for a, f in lst_to_turn_sort:
            self.to_turn.append(f['id'])

        return lst_to_turn
        
    def turn_faces_0(self):
        bb = self.get_biggest_face() 
        # bb = 39
        logging.info('turn around bb')
        self.ff = [bb]
        self.turn_faces(self.faces[bb])

        for f in self.ff[1:]:
            self.turn_faces(self.faces[f])

    def align(self):
        self.to_turn = [int(len(self.faces)/2)]
        #self.to_turn = [39, 75, 118]
        self.cpt = 1            
        while self.cpt < len(self.faces) and len(self.to_turn) != 0:
            to_turn_now = self.to_turn[:]
            logging.debug('it %s, %s faces to turn'%(self.cpt, len(to_turn_now)))
            self.to_turn = []
            for f in to_turn_now:
                self.turn_faces(self.faces[f])
        
            # if self.cpt == 141:
            #     break

    def get_common_edge(self, face1, face2):
        for i in face1['edges']:
            if i in face2['edges']:
                return i

    def get_connected_face(self, dic_face, edge):
        id_face = dic_face['id']
        return [self.faces[i]['id'] for i in edge['id_faces'] if i != id_face][0]

    def get_connected_faces(self):
        
        logging.info('get connected faces')

        for i in self.faces:
            i['id_faces'] = []
            for e in i['edges']:
                edge = self.edges[e]
                if not edge['bord'] and not set(edge['facets']).issubset(set(i['facets'])):
                    i['id_faces'].append(self.get_connected_face(i, edge))

    def proceed_geometry(self):
        self.get_edges_from_facets()
        self.get_edges_angle()
        self.mesh_to_part()
        self.link_edges_and_faces()
        self.apply_transforms()
        self.project_on_xy()
        #self.get_connected_faces()


    def prepare_cut_and_fuse(self):
        for dic_face in self.faces:
            dic_face['to_cut'] = []
            dic_face['to_fuse'] = []
            dic_face['ep_cut'] = 1
            dic_face['ep_fuse'] = 0.5
        
        for dic_edge in self.edges.values():
            dic_edge['first'] = True
            (dic_edge['ep_cut'], dic_edge['ep_fuse']) = self.get_ep(dic_edge)

    def get_ep(self, dic_edge):
        """
        return thickness for cut and fuse 
        depends on the angle and the thickness of the material
        """
        if self.method == 0:
            return slot(self.material_thickness, dic_edge['angle'])
        elif self.method == 1:
            return slot_2(self.material_thickness, dic_edge['angle'], dic_edge['valley'])

    def get_cut_and_fuse(self):
        for i, dic_face in enumerate(self.faces):
            self.get_cut_and_fuse_face(dic_face)


    def get_cut_and_fuse_face(self, dic_face):
        for i, edge_name in enumerate(dic_face['edges']):
            edge = self.edges[edge_name]
            i_edge = dic_face['edges_part'][i]
            
            ep_cut = edge['ep_cut']
            ep_fuse = edge['ep_fuse']
            
            if edge['first']:
                first = True
                edge['first'] = False
            else:
                first = False

            if not edge['bord'] and 'flat' not in edge:
                (to_cut, to_fuse) = self.get_cut_and_fuse_edge(dic_face, 
                                                               i_edge, 
                                                               ep_cut, 
                                                               ep_fuse, 
                                                               first)
                if to_cut: 
                    dic_face['to_cut'].append(to_cut)
                if to_fuse:
                    dic_face['to_fuse'].append(to_fuse)

        if self.clear_angle_radius > 0:
            for dic_face in self.faces:
                part2 = dic_face['part2']

                for v in part2.Vertexes:
                    c = Part.makeCircle(self.clear_angle_radius, v.Point)
                    dic_face['to_cut'].append(Part.Face(Part.Wire(c)))

    def get_cut_and_fuse_edge(self, dic_face, i_edge, ep_cut, ep_fuse, first):
        first_cut = False
        first_fuse = True
        # if first:
        #     first_cut = True
        #     first_fuse = False
        return (self.get_slots(dic_face, i_edge, -ep_cut, first_cut), 
                self.get_slots(dic_face, i_edge, ep_fuse, first_fuse))

    def get_slots(self, dic_face, i_edge, ep, first):
        """
        get a Part objects containing slots or teeth for the
        edge -i_edge- of the face -dic_face-
        
        - ep is the thickness of the slot
            if ep > 0, the returned geometry is a set of teeth to fuse with the face
            if ep < 0, the returned geometry is a set of slots to cut off the face
        - first is a boolean: True if it is the first time this edge is cut 
        """
        if ep == 0:
            return None

        edge = dic_face['part2'].Edges[i_edge]
        vec_edge = edge.Vertexes[1].Point - edge.Vertexes[0].Point
        l_tot = vec_edge.Length
        l_slot = l_tot/self.n
        n_slot = self.n/2
        slot_poly = Part.makePolygon([(0     ,  0, 0), 
                                      (l_slot,  0, 0), 
                                      (l_slot, ep, 0), 
                                      (0     , ep, 0), 
                                      (0     , 0 , 0)])

        if ep<0:
            if self.drill_radius > 0:
                d = self.drill_radius
                slot_poly = Part.makePolygon([(0         , 0         , 0), 
                                              (l_slot    , 0         , 0), 
                                              (l_slot    , ep - (d/2), 0), 
                                              (l_slot - d, ep - (d/2), 0), 
                                              (l_slot - d, ep        , 0), 
                                              (d         , ep        , 0),
                                              (d         , ep - (d/2), 0), 
                                              (0         , ep - (d/2), 0), 
                                              (0         , 0         , 0)])

                slot_poly_end = Part.makePolygon([(0         , 0         , 0), 
                                                  (l_slot*1.5, 0         , 0), 
                                                  (l_slot*1.5, ep        , 0), 
                                                  (d         , ep        , 0),
                                                  (d         , ep - (d/2), 0), 
                                                  (0         , ep - (d/2), 0), 
                                                  (0         , 0         , 0)])
            else:
                slot_poly_end = Part.makePolygon([(0         , 0 , 0), 
                                                  (l_slot*1.5, 0 , 0), 
                                                  (l_slot*1.5, ep, 0), 
                                                  (0         , ep, 0), 
                                                  (0         ,  0, 0)])

            slot_wire_end = Part.Wire(slot_poly_end)
        slot_wire = Part.Wire(slot_poly)

        lst_slot = []
        for i in range(n_slot):
            if first:
                start = 0
            else:
                start = 1
            slot = Part.Face(slot_wire)
            slot.translate(((i*2+start)*l_slot, 0, 0))
            lst_slot.append(slot)

        if ep<0:
            slot = Part.Face(slot_wire_end)
            slot.translate((((n_slot-1)*2+start)*l_slot, 0, 0))
            lst_slot[-1] = slot

        slots = Part.makeCompound(lst_slot)
        angle_rot = vec_edge.getAngle(V_X) * 180/PI
        if V_X.cross(vec_edge)[2] < 0 :
            angle_rot = -angle_rot
        slots.rotate(V_O, V_Z, angle_rot)
        slots.translate(edge.Vertexes[0].Point)
        return slots

    def proceed_cut_and_fuse(self):

        logging.info('prepare cut and fuse')

        self.prepare_cut_and_fuse()
        self.get_cut_and_fuse()

        logging.info('proceed cut and fuse')

        for dic_face in tqdm(self.faces, leave = True, ascii=True):
            part_out = dic_face['part2'].copy()
            if len(dic_face['to_fuse'])>0:
                dic_face['fuse_object'] = Part.makeCompound(dic_face['to_fuse'])
                part_out = part_out.fuse(dic_face['fuse_object'])

            if len(dic_face['to_cut'])>0:
                #dic_face['cut_object'] = Part.makeCompound(dic_face['to_cut'])
                for cut in dic_face['to_cut']:
                    part_out = part_out.cut(cut)

            part_out = part_out.removeSplitter()
            dic_face['part_out'] = part_out.transformGeometry(self.matrix_scale)
            #dic_face['part_out'] = part_out
        logging.info('done cut and fuse')


    def add_edge_number_face(self, dic_face, text = True):
        dic_face['edges_shape'] = []
        center = dic_face['part2'].CenterOfMass
        l_edges = [i for i in dic_face['edges'] if 'flat' not in self.edges[i]] 
        for edge in l_edges:
            dic_edge = {}
        
            edge_p2 = self.get_edge_part2(dic_face, edge)
            edge_n = self.edges[edge]['id_edge']

            # get the numero of the second face connected to this edge
            faces = self.edges[edge]['id_faces']
            face_n = ''
            if len(faces) == 2:
                if faces[0] == dic_face['id']:
                    face_n = faces[1]
                else:
                    face_n = faces[0]
                face_n = self.faces[int(face_n)]['numero']

            pts = [i.Point for i in edge_p2.Vertexes]
            pos_txt = pts[0] + (pts[1]-pts[0])*0.5
            direc = (center-pos_txt).normalize()
            pos_txt = pos_txt +  direc * 5
            angle = V_X.getAngle(pts[1]-pts[0])
            if (pts[1]-pts[0]).cross(V_X)[2] > 0 :
                angle = -angle

            if text:
                d = Draft.makeText(str(face_n), pos_txt)
            else:
                d = Draft.makeShapeString(str(face_n), FONTPATH, 3)
                d.Placement.move(pos_txt)
                
            if len(self.edges[edge]['facets']) == 2:
                dic_edge['face_n'] = str(face_n)
                dic_edge['edge_n'] = str(edge_n)
                dic_edge['pos'] = pos_txt
                dic_edge['shape'] = d
                dic_edge['angle'] = angle
                dic_edge['valley'] = self.edges[edge]['valley']
                dic_face['edges_shape'].append(dic_edge)
            
    def add_edge_number(self):
        for face in self.faces:
            self.add_edge_number_face(face)

    def reconstruct_3d(self, filename=None):
        for f in self.faces:
            p = get_biggest(f['part_out'].Faces).copy()
            tr = -self.material_thickness/2.0
            if self.method == 0:
                p.translate(V_Z * tr)
                p = p.extrude(V_Z * self.material_thickness)
            elif self.method == 1:
                p = p.extrude(V_Z * -self.material_thickness)
            p.translate(V_Z * f['z0'])
            #p.translate(FreeCAD.Vector(0,0, 10))
            #print f['z0']
            t = f['transform']
            if t['angle_rot'] != 0 and t['vec_rot'].Length !=0 :
                p.rotate(t['point_rot'],
                         t['vec_rot'],
                         -t['angle_rot'])
            f['part_orig'] = Part.Solid(p)
            Part.show(p)
            if filename:
                self.active_document.saveAs(filename)

    def debug_3d(self, filename = None):
        logging.info('starting 3d debug')
        lst_c = []
        for i, it in enumerate(self.faces):
            for j in tqdm(self.faces[i+1:], desc='face %s'%i, leave=True, ascii=True):
                if it['numero'] not in j['id_faces']:
                    try:
                        c = it['part_orig'].common(j['part_orig'])
                        if c.Volume > 0.5:
                            lst_c.append(c)
                    except:
                        pass
        for c in lst_c:
            Part.show(c)
        self.debug_lst = lst_c
        if filename:
            self.active_document.saveAs(filename)

    def debug_map_edge(self, filename):
        for i in self.faces:
            if len(i['edges']) != len(i['edges_part']):
                Part.show(i['part0'])
        for i in self.faces:
            Part.show(i['part0'])



        self.active_document.saveAs(filename)
            

    def export_txt(self, export_number = True):
        s = self.export_scale

        if export_number:
            self.add_edge_number()

        txt = ''
        for face in self.faces:
            txt += '<g>\n'
            part = face['part_out']
            if len(part.childShapes()) > 1:
                shape = get_biggest(part.childShapes())
            else:
                shape = part
            lst_points = [i.Point for i in shape.Vertexes]
            
            face['draft'] = Draft.makeWire(shape.Wires[0], closed = True)
            txt += Draft.getSVG(face['draft']).replace('#888888', '#888888;fill-opacity:0.5')
            for i in face['edges_shape']:

                txt += '<g transform="rotate(%s, %s, %s)">'%(round(i['angle']*180/math.pi,2),
                                                             round(i['pos'].x*s,2), 
                                                             round(i['pos'].y*s, 2))
                edge_txt = i['face_n']
                if i['valley']:
                    edge_txt += 'v'
                    
                #txt += add_text_text(round(i['pos'].x*s,2), round(i['pos'].y*s, 2), edge_txt)
                txt += self.add_text(round(i['pos'].x*s,2), round(i['pos'].y*s, 2), edge_txt, 0.5)
                txt += '</g>\n'
                
            c = face['part2'].CenterOfMass            
            txt += self.add_text(round(c.x*s,2), round(c.y*s, 2), str(face['numero']), 1)

            txt += '</g>\n'
        return txt

    def export(self, f_out):
        logging.info('create .svg file')

        txt = EMPTY_SVG
        # with open(EMPTY_SVG, 'r') as f:
        #     txt = f.read()
        txt += self.export_txt()
        txt += '</g>\n</svg>'

        with open(f_out, 'w') as f:
            f.write(txt)

        logging.info('svg export successful')

    def print_summary(self):
        s =  0
        min_s = 1000000
        max_s = 0
        for i in self.faces:
            s_n = i['part2'].Area
            s += s_n
            min_s = min(min_s, s_n)
            max_s = max(max_s, s_n)


        if s > 1000000:
            rap = 1000000
        else:
            rap = 1
        logging.info('====== SUMMARY =====')
        logging.info('\tfaces:\t %s'%len(self.faces))
        logging.info('\tmin area:\t %s'%round(min_s/rap, 2))
        logging.info('\tmax area:\t %s'%round(max_s/rap, 2))
        logging.info('\ttot area:\t %s'%round(s/rap, 2))

    def show(self):
        for i in self.faces:
            Part.show(i['part_out'])


if TEST:
    m = Mesh.read('/home/laurent/Projets/Buste/homme/homme_low_poly_2.stl')
    ply = PlywoodExport(m, scale = 1)
    ply.proceed_geometry()
    ply.proceed_cut_and_fuse()
    ply.reconstruct_3d()
#    ply.debug_3d()
#    for i in ply.faces:
#        Part.show(i['part0'])
#    ply.get_connected_faces()
#    ply.align()
#    for i in ply.faces:
#        Part.show(i['part2'])
#    ply.add_edge_number()
#    for i in ply.faces:
#        for j in i['edges_shape']:
#            Part.show(j.Shape)

if __name__=='__main__' and not TEST:
    arguments = docopt(__doc__)
    type_text = 'hershey'
    for i, it in arguments.items():
        logging.debug('docopt: %s = %s'%(i, it))

    if arguments['--no-hershey']:
        type_text = 'text'

    if arguments['--test']:

        m = Mesh.read('/home/laurent/Projets/PlywoodExport/test/goutte.stl')
        ply = PlywoodExport(m)
        ply.proceed_geometry()
        ply.get_connected_faces()
        ply.align()
#        for i in ply.faces:
#            Part.show(i['part2'])
        ply.add_edge_number()
    elif arguments['--debug'] or arguments['--reconstruct']:
        m = Mesh.read(arguments['INPUT'])
        ply = PlywoodExport(m,
                            scale = 1,
                            nslots = int(arguments['--nslots']), 
                            thickness = float(arguments['--thickness']),
                            clear_angle_radius = float(arguments['--clear-radius']), 
                            drill_radius = float(arguments['--drill-radius']))
        ply.proceed_geometry()
        ply.proceed_cut_and_fuse()
        ply.reconstruct_3d(arguments['--reconstruct'])
        ply.get_connected_faces()
        #ply.align()
        #ply.export(arguments['OUTPUT'])
        if arguments['--debug']:
            ply.debug_3d(arguments['--debug'])
        #ply.active_document.saveAs(arguments['OUTPUT'])
        ply.print_summary()
    elif arguments['--stat']:
        m = Mesh.read(arguments['INPUT'])
        ply = PlywoodExport(m,
                            scale = 1,
                            nslots = int(arguments['--nslots']), 
                            thickness = float(arguments['--thickness']),
                            clear_angle_radius = float(arguments['--clear-radius']), 
                            drill_radius = float(arguments['--drill-radius']))
        ply.proceed_geometry()
        ply.proceed_cut_and_fuse()
        ply.print_summary()
        
    else:
        m = Mesh.read(arguments['INPUT'])
        ply = PlywoodExport(m,
                            thickness = float(arguments['--thickness']),
                            nslots = int(arguments['--nslots']), 
                            output = arguments['OUTPUT'],
                            clear_angle_radius = float(arguments['--clear-radius']),
                            type_text = type_text,
                            drill_radius = float(arguments['--drill-radius']))
#    ply.show()
    
